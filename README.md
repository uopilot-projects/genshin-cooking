Идеальная готовка
=================

[Видео на YouTube](https://www.youtube.com/watch?v=IRAEG4gM8V0)


Данный скрипт позволяет автоматизировать процесс ручной готовки в игре Genshin Impact.

Скрипт рассчитан на разрешение экрана и игры 1920×1080. При других параметрах нуждается в корректировке.

    // Genshin impact Идеальная готовка
    set #b 0
    set #color 4243711
    set #color2 12648447
    while #b = 0
    set #b findcolor (512, 691 1292, 869 #color %arr 2 abs)
    end_while
    sort_array %arr
    while %arr[1 1], %arr[1 2] #color
        wait 0
    end_while
    wait 25
    kleft 958, 944 abs